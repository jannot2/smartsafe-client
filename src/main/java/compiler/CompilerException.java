package compiler;

public class CompilerException extends Exception {
	private static final long serialVersionUID = -7999890519466851106L;

	public CompilerException(String msg) {
		super(msg);
	}
}
